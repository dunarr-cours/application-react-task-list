import React, { useState } from 'react';
import './App.css';
import tasks from './tasks.json'

function App() {
  function displayTasks(){
    return tasks.map((task,  id) => <li key={id}>{task}</li>)
  }
  return (
    <ul>
      {displayTasks()}
    </ul>
  );
}

export default App;
